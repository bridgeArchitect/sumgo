// appGo project main.go
package main

import (
	"log"
	"strconv"

	"github.com/gotk3/gotk3/gtk"
)

var (
	win                      *gtk.Window
	entryA, entryB, entrySum *gtk.Entry
	sumButton                *gtk.Button
	builder                  *gtk.Builder
	mainWindow               *gtk.Window
	errorWindow              *gtk.Window
	errorLabel               *gtk.Label
	successAdd               bool
	errorImageA, errorImageB *gtk.Image
)

func checkErrorFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func checkErrorWindow(err error) {

	if err != nil {
		if errorWindow != nil {
			errorWindow.Destroy()
		}
		errorWindow = createWindow("errorWindow.glade", "errorWindow")
		errorWindow.SetName("errorWindow")
		errorLabel = createObject("errorLabel").(*gtk.Label)
		errorLabel.SetText(err.Error())
		errorWindow.ShowAll()
		successAdd = false
	}

}

func createWindow(nameGlade, nameWindow string) *gtk.Window {

	var err error
	err = builder.AddFromFile(nameGlade)
	checkErrorFatal(err)

	obj, err := builder.GetObject(nameWindow)
	checkErrorFatal(err)
	win = obj.(*gtk.Window)

	return win

}

func createObject(nameObject string) interface{} {

	obj, err := builder.GetObject(nameObject)
	checkErrorFatal(err)
	return obj

}

func main() {

	var err error

	gtk.Init(nil)
	builder, err = gtk.BuilderNew()
	checkErrorFatal(err)

	mainWindow = createWindow("mainWindow.glade", "mainWindow")
	mainWindow.SetName("mainWindow")
	_, err = mainWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})
	checkErrorFatal(err)

	entryA = createObject("entryA").(*gtk.Entry)
	entryB = createObject("entryB").(*gtk.Entry)
	entrySum = createObject("entrySum").(*gtk.Entry)
	sumButton = createObject("sumButton").(*gtk.Button)
	errorImageA = createObject("errorImageA").(*gtk.Image)
	errorImageB = createObject("errorImageB").(*gtk.Image)

	_, err = mainWindow.Connect("show", func() {
		entryA.SetText("0.0")
		entryB.SetText("0.0")
		entrySum.SetText("0.0")
		errorImageA.SetVisible(false)
		errorImageB.SetVisible(false)
	})
	checkErrorFatal(err)

	_, err = entryA.Connect("changed", func() {

		var (
			errText, errParse error
			textA             string
		)

		textA, errText = entryA.GetText()
		_, errParse = strconv.ParseFloat(textA, 64)

		if errText != nil || errParse != nil {
			errorImageA.SetVisible(true)
		} else {
			errorImageA.SetVisible(false)
		}

	})
	checkErrorFatal(err)

	_, err = entryB.Connect("changed", func() {

		var (
			errText, errParse error
			textB             string
		)

		textB, errText = entryB.GetText()
		_, errParse = strconv.ParseFloat(textB, 64)

		if errText != nil || errParse != nil {
			errorImageB.SetVisible(true)
		} else {
			errorImageB.SetVisible(false)
		}

	})
	checkErrorFatal(err)

	_, err = sumButton.Connect("clicked", func() {

		var (
			textA, textB string
			err          error
			a, b, answer float64
		)

		successAdd = true

		textA, err = entryA.GetText()
		checkErrorWindow(err)
		textB, err = entryB.GetText()
		checkErrorWindow(err)

		a, err = strconv.ParseFloat(textA, 64)
		checkErrorWindow(err)
		b, err = strconv.ParseFloat(textB, 64)
		checkErrorWindow(err)

		if successAdd {
			answer = a + b
			entrySum.SetText(strconv.FormatFloat(answer, 'f', 10, 64))
			if errorWindow != nil {
				errorWindow.Destroy()
			}
		}

	})
	checkErrorFatal(err)

	mainWindow.ShowAll()
	gtk.Main()

}
